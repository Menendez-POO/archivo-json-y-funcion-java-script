
const expresiones = {
    cedula: /^[^a-z][0-9]{10}$/,
    nombre: /^[a-zA-ZÀ-ÿ\s]{1,30}$/, 
    telefono: /^\d{10}$/, 
    correo: /\S+@\S+\.\S+/ 
};

const validForm = (e) => {

    switch (e.target.name) {


        case "cedula":
            if(expresiones.apelli.test(e.target.value)){
                document.getElementById("smserr1").innerHTML = "";
                b = false;
            } else{
                document.getElementById("smserr1").innerHTML = "No ingrese datos tipo texto / Campo en blanco";
                b = true;
            }
        break;

        case "nombre":
            if(expresiones.nombre.test(e.target.value)){
                document.getElementById("smserr2").innerHTML = "";
                a = false;
            } else{
                document.getElementById("smserr2").innerHTML = "No ingrese datos numericos / Campo en blanco";
                a = true;
            }
        break;

        case "telefono":
            if(expresiones.tele.test(e.target.value)){
                document.getElementById("smserr3").innerHTML = "";
                d = false;
            } else{
                document.getElementById("smserr3").innerHTML = "Su telefono debe tener 10 caracteres numericos";
                d = true;
            }
        break;

        case "correo":
            if(expresiones.correo.test(e.target.value)){
                document.getElementById("smserr4").innerHTML = "";
                c = false;
            } else{
                document.getElementById("smserr4").innerHTML = "Formato de correo no valido";
                c = true;
            }
        break;

    }
    
 }
     




